# ssh-client

A simple Docker image with an SSH client, inspired by https://hub.docker.com/r/kroniak/ssh-client/, including rsync.
